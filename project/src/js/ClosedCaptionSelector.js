
var ClosedCaptionSelector ={

    initialise: function () {
        ClosedCaptionSelector.events.initialise();
        $('#jsCCOffTick').show();
        $('#jsCCOnTick').hide();
    },

    setClosedCaptions: function(value){
        var captionsOn = VideoPlayerInterface.iframeWindow.rtc.player.vars.showCaptions;
        if(value === 'on' && !captionsOn){
            VideoPlayerInterface.iframeWindow.rtc.player.toggleCC();
            $('#jsCCOnTick').show();
            $('#jsCCOffTick').hide();
            $('.js-cc-text').text(' (On)');
        } else if (value === 'off' && captionsOn) {
            VideoPlayerInterface.iframeWindow.rtc.player.toggleCC();
            $('#jsCCOffTick').show();
            $('#jsCCOnTick').hide();
            $('.js-cc-text').text(' (Off)');
        }
    },

    events: {
        /**
         * Link up the events and the event handlers
         */
        initialise: function() {
            $('#jsCCMenuTitle').click(ClosedCaptionSelector.events.closeCCMenu);
            $('.timeline-control-cc__item').click(ClosedCaptionSelector.events.ccItemClickEventHandler);

        },

        closeCCMenu: function (e) {
            $('#jsSettingsButtonPopout').show();
            $('#jsCCSelectorPopout').hide();
        },

        ccItemClickEventHandler: function(e) {
            //off or on
            var newValue = $(this).data('value');
            ClosedCaptionSelector.setClosedCaptions(newValue);
        }
    }
};